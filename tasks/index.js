const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))

module.exports = async () => {
  const listeners = await fs.readdirAsync(`${__dirname}/listeners`)
  const jobs = await fs.readdirAsync(`${__dirname}/jobs`)

  try {
    for (let i = 0; i < listeners.length; i++) {
      require(`${__dirname}/listeners/${listeners[i]}`)()
    }

    for (let i = 0; i < jobs.length; i++) {
      require(`${__dirname}/jobs/${jobs[i]}`)
    }
  } catch (err) {
    throw new Error(err)
  }
}
