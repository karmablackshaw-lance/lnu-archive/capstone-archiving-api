/**
 * TODO - Describe what this test does in general.
 * @Test Module Students
*/

const students = require(`../store/students`)
const assert = require('assert')
const faker = require('faker')

describe(`Students`, function() {

  /**
   * Description - Get all `students`
   * @action  'students/'
   */
  describe('index', function() {
    it(`should return Students`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Get all `students`
   * @action  'students/'
   */
  describe('store', function() {
    it(`should store Students`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Get specific `students
   * @action  'students/:students_id'
   */
  describe('show', function() {
    it(`should show Students`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Update `students`
   * @action  'students/:students_id'
   */
  describe('update', function() {
    it(`should update Students`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Remove  `students`
   * @action  'students/:students_id'
   */
  describe('destroy', function() {
    it(`should destroy Students`, function() {
      assert.equal(true, true)
    })
  })

})