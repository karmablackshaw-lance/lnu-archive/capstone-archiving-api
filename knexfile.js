require('dotenv').config()

const client = process.env.DB_CLIENT || 'mysql'
const host = process.env.DB_HOST || '127.0.0.1'
const user = process.env.DB_USER || 'root'
const password = process.env.DB_PASS || ''
const database = process.env.DB_NAME || 'p2p_trade_test'
const charset = process.env.DB_CHARSET || 'utf8'

const typeCast = (field, next) => {
  try {
    if (field.type === 'BIT' && field.length === 1) {
      const bytes = field.buffer()
      return bytes ? (bytes[0] === 1) : null
    }

    return next()
  } catch (err) {
    console.log(err)
  }
}

module.exports = {

  development: {
    client: client || 'mysql',
    connection: {
      host,
      user,
      password,
      database,
      charset,
      dateStrings: true,
      typeCast
    }
  },

  staging: {
    client: client || 'mysql',
    connection: {
      host,
      user,
      password,
      database,
      charset,
      dateStrings: true,
      typeCast
    }
  },

  production: {
    client: client || 'mysql',
    connection: {
      host,
      user,
      password,
      database,
      charset,
      dateStrings: true,
      typeCast
    }
  }
}
