const { verify } = require('@utilities/jwt')

module.exports = ({ except } = {}) => {
  return async (ctx, next) => {
    if (Array.isArray(except) && ctx.matched.find(x => except.includes(x.name))) {
      return next()
    }

    if (ctx._matchedRouteName === 'unauth') {
      return next()
    }

    try {
      const token = ctx.request.headers.authorization

      const splitToken = token && token.split(' ')
      if (!splitToken || splitToken.length !== 2) {
        throw new Error('ACCESS_DENIED')
      }

      const verification = await verify(splitToken[1], process.env.APP_KEY)

      ctx.user = verification.data

      return next()
    } catch (error) {
      ctx.throw(401)
    }
  }
}
