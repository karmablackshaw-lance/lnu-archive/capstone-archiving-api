const _isNil = require('lodash/isNil')
const _toLower = require('lodash/toLower')
const { toArray } = require('@utilities/helpers')

module.exports = () => {
  return async (ctx, next) => {
    ctx.getDefaults = ({ page = 1, rows = 15 } = {}) => {
      const query = ctx.request.query
      const toArrayCustom = x => _isNil(x) ? [] : toArray(x)
      const arrayParamsWrapper = key => toArrayCustom(query[`${key}[]`] || query[key]).map(_toLower)

      return {
        q: arrayParamsWrapper('q'),
        filterBy: arrayParamsWrapper('filterBy'),
        sort: arrayParamsWrapper('sort'),
        sortBy: arrayParamsWrapper('sortBy'),
        dateBy: arrayParamsWrapper('dateBy'),
        dateFrom: arrayParamsWrapper('dateFrom'),
        dateTo: arrayParamsWrapper('dateTo'),
        dataStatus: query.dataStatus ? _toLower(query.dataStatus) : undefined,
        page: query.page || page,
        rows: query.rows || rows
      }
    }

    return next()
  }
}
