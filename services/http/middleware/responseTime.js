module.exports = () => {
  return async (ctx, next) => {
    try {
      const start = Date.now()
      await next()
      const ms = Date.now() - start
      ctx.set('X-Response-Time', `${ms}ms`)
    } catch (err) {
      console.log(err)
    }
  }
}
