const Koa = require('koa')
const app = new Koa()
const routes = require('./router')
const cors = require('@koa/cors')
const bodyParser = require('koa-bodyparser')
const formidable = require('koa2-formidable')
const serve = require('koa-static')

// Custom Middlewares
const responseTime = require('./middleware/responseTime')
const errorHandler = require('./middleware/errorHandler')
const params = require('./middleware/params')

module.exports = async () => {
  const router = await routes()

  app.proxy = true

  app.use(cors())
  app.use(router.allowedMethods())
  app.use(formidable())
  app.use(bodyParser())
  app.use(serve('./assets'))

  app.use(responseTime())
  app.use(errorHandler())
  app.use(params())

  app.use(router.routes())
  app.listen(process.env.PORT || '4000')

  console.log(`Services loaded | Listening on port ${process.env.PORT || 4000}`)

  return app
}
