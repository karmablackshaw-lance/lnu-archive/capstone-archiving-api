/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Authors
*/

const authors = require('@store/authors')
const router = require('koa-router')()
const Joi = require('joi')

router
  .prefix('/authors')

// authentication

  .get('/dashboard', async (ctx, next) => {
    try {
      ctx.body = await authors.dashboard({
        ...ctx.getDefaults(ctx)
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/', async (ctx, next) => {
    try {
      ctx.body = await authors.index({
        ...ctx.getDefaults(ctx)
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/publishers', async (ctx, next) => {
    try {
      ctx.body = await authors.publishersList({
        ...ctx.getDefaults(ctx)
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/public-archive', async (ctx, next) => {
    try {
      ctx.body = await authors.publicArchive({
        ...ctx.getDefaults(ctx)
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/', async (ctx, next) => {
    const schema = Joi.object({
      author: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await authors.store(request)
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/:id', async (ctx, next) => {
    try {
      ctx.body = await authors.show(ctx.params.id)
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/', async (ctx, next) => {
    const schema = Joi.object({
      id: Joi.number().required(),
      author: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await authors.update({
        authorID: request.id,
        name: request.author
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .delete('/', async (ctx, next) => {
    const schema = Joi.object({
      author_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await authors.deleteAuthor({
        authorID: request.author_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/restore', async (ctx, next) => {
    const schema = Joi.object({
      author_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await authors.restoreAuthor({
        authorID: request.author_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

module.exports = router
