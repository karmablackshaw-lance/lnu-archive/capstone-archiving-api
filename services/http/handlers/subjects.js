/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Subjects
*/

const subjects = require('@store/subjects')
const router = require('koa-router')()
const Joi = require('joi')

router
  .prefix('/subjects')

  .get('/dashboard', async (ctx, next) => {
    ctx.body = await subjects.dashboard({
      ...ctx.getDefaults()
    })
  })

  .get('/sub', async (ctx, next) => {
    ctx.body = await subjects.subSubjectsList({
      ...ctx.getDefaults()
    })
  })

  .get('/public-archive', async (ctx, next) => {
    ctx.body = await subjects.publicArchive({
      ...ctx.getDefaults()
    })
  })

  .get('/home', async (ctx, next) => {
    ctx.body = await subjects.homeList({
      ...ctx.getDefaults()
    })
  })

  .get('/', async (ctx, next) => {
    ctx.body = await subjects.subjectsList(ctx.request.query)
  })

  .post('/', async (ctx, next) => {
    ctx.body = await subjects.store(ctx.request.body)
    next()
  })

  .post('/sub', async (ctx, next) => {
    const schema = Joi.object({
      subject_id: Joi.number().required(),
      name: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.createSubSubject({
        subjectID: request.subject_id,
        name: request.name
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/:id', async (ctx, next) => {
    ctx.body = await subjects.show(ctx.params.id)
    next()
  })

  .put('/', async (ctx, next) => {
    const schema = Joi.object({
      subject_id: Joi.number().required(),
      subject: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.updateSubject({
        subjectID: request.subject_id,
        subject: request.subject
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/sub', async (ctx, next) => {
    const schema = Joi.object({
      subject_id: Joi.number().required(),
      sub_id: Joi.number().required(),
      name: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.updateSubSubject({
        subjectID: request.subject_id,
        subID: request.sub_id,
        name: request.name
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .delete('/', async (ctx, next) => {
    const schema = Joi.object({
      subject_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.deleteSubject({
        subjectID: request.subject_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .delete('/sub', async (ctx, next) => {
    const schema = Joi.object({
      sub_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.deleteSubSubject({
        subID: request.sub_id
      })
    } catch (error) {
      ctx.throw(error)
    }
    next()
  })

  .put('/restore', async (ctx, next) => {
    const schema = Joi.object({
      subject_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.restoreSubject({
        subjectID: request.subject_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/restore/sub', async (ctx, next) => {
    const schema = Joi.object({
      sub_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await subjects.restoreSubSubject({
        subID: request.sub_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

module.exports = router
