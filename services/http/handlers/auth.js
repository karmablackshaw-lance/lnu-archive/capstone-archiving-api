/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Auth
*/

const auth = require('@store/auth')
const router = require('koa-router')()
const Joi = require('joi')

// middlewares
const authentication = require('@middleware/authentication')
const JWT = require('@utilities/jwt')

router
  .prefix('/auth')

  .use(authentication())

  .get('/', async (ctx, next) => {
    ctx.body = await auth.index(ctx.request.query)
    next()
  })

  .post('/revalidate', async ctx => {
    try {
      ctx.body = await JWT.sign(ctx.user)
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('unauth', '/', async ctx => {
    try {
      const schema = Joi.object({
        username: Joi.string().required(),
        password: Joi.string().required()
      })

      const request = await schema.validateAsync({
        username: ctx.request.body.username,
        password: ctx.request.body.password
      })

      ctx.body = await auth.login({
        username: request.username,
        password: request.password
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/:id', async (ctx, next) => {
    ctx.body = await auth.show(ctx.params.id)
    next()
  })

  .put('/:id', async (ctx, next) => {
    ctx.body = await auth.update(ctx.params.id)
    next()
  })

  .delete('/:id', async (ctx, next) => {
    ctx.body = await auth.destroy(ctx.params.id)
    next()
  })

module.exports = router
