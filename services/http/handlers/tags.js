/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Tags
*/

const tags = require('@store/tags')
const router = require('koa-router')()
const Joi = require('joi')

// middlewarese
const authentication = require('@middleware/authentication')

router
  .prefix('/tags')

  .use(authentication())

  .get('/dashboard', async (ctx, next) => {
    try {
      ctx.body = await tags.dashboard({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/', async (ctx, next) => {
    try {
      ctx.body = await tags.index({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('unauth', '/popular', async (ctx, next) => {
    try {
      ctx.body = await tags.homeList({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('unauth', '/public-archive', async (ctx, next) => {
    try {
      ctx.body = await tags.publicArchive({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/', async (ctx, next) => {
    const schema = Joi.object({
      tag: Joi.string().required(),
      definition: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)
      ctx.body = await tags.store(request)
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/:id(\\d+)', async (ctx, next) => {
    ctx.body = await tags.show(ctx.params.id)
    next()
  })

  .put('/', async (ctx, next) => {
    const schema = Joi.object({
      tag_id: Joi.number().required(),
      tag: Joi.string().required(),
      definition: Joi.string().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await tags.update({
        tagID: request.tag_id,
        tag: request.tag,
        definition: request.definition
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .delete('/', async (ctx, next) => {
    const schema = Joi.object({
      tag_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await tags.deleteTag({
        tagID: request.tag_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/restore', async (ctx, next) => {
    const schema = Joi.object({
      tag_id: Joi.number().required()
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await tags.restoreTag({
        tagID: request.tag_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

module.exports = router
