const router = require('koa-router')()

const Users = require('@store/users')

// libraries
const Joi = require('joi')
const _get = require('lodash/get')

// middlewarese
const authentication = require('@middleware/authentication')

// utilities
const bcrypt = require('@utilities/bcrypt')
const buffer = require('@utilities/buffer')

router
  .prefix('/users')

  .use(authentication())

  .get('/', async ctx => {
    try {
      const params = {
        ...ctx.getDefaults(),
        status: ctx.request.query.status
      }

      const count = await Users.index({ ...params, isCount: 1 })
      const list = await Users.index({ ...params })

      ctx.body = { count: _get(count, 'total', 0), list }
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('/authenticated', async ctx => {
    try {
      const find = await Users.findUserBy('user_id', ctx.user.id)

      if (!find) {
        ctx.throw(401)
      }

      ctx.body = find
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('unauth', '/dashboard', async ctx => {
    try {
      ctx.body = await Users.dashboard()
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('unauth', '/', async ctx => {
    const nameRegex = /^[A-Za-z\s]+$/

    const schema = Joi.object({
      fname: Joi.string()
        .pattern(nameRegex)
        .required()
        .messages({
          'string.pattern.base': 'First Name should contain only letters'
        }),
      mname: Joi.string()
        .pattern(nameRegex)
        .required()
        .messages({
          'string.pattern.base': 'Middle Name should contain only letters'
        }),
      lname: Joi.string()
        .pattern(nameRegex)
        .required()
        .messages({
          'string.pattern.base': 'Last Name should contain only letters'
        }),
      username: Joi.string().required(),
      password: Joi.string().required()
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      const findUser = await Users.findBy({
        fname: data.fname,
        mname: data.mname,
        lname: data.lname,
        username: data.username
      })

      if (findUser) {
        ctx.throw(422, 'User already registered')
      }

      ctx.body = await Users.store(data)
    } catch (error) {
      console.log(error)
      ctx.throw(error)
    }
  })

  .patch('/', async ctx => {
    const schema = Joi.object({
      user_id: Joi.number()
        .required(),
      status: Joi.string()
        .valid('pending', 'confirmed', 'rejected')
        .required()

    })
    try {
      const request = await schema.validateAsync(ctx.request.body)

      ctx.body = await Users.update(request)
    } catch (error) {
      ctx.throw(error)
    }
  })

  .patch('/authenticated', async ctx => {
    const schema = Joi.object({
      color: Joi.string()
        .required(),
      fname: Joi.string()
        .required(),
      level: Joi.string()
        .required(),
      lname: Joi.string()
        .required(),
      mname: Joi.string()
        .required(),
      user_id: Joi.number()
        .required(),
      username: Joi.string()
        .required(),
      password: Joi.string()
        .optional(),
      passwordConfirmation: Joi.string()
        .optional()
        .equal(Joi.ref('password')).strip()
        .messages({
          'any.only': 'Please confirm your password'
        })
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      if (data.password) {
        data.password = await bcrypt.hash(data.password)
        data.temp = buffer.toBase64(data.password)
      }

      ctx.body = await Users.update(data)
    } catch (error) {
      console.log(error)
      ctx.throw(error)
    }
  })

  .delete('/', async ctx => {
    console.log('delete', ctx.request.body)
    ctx.body = await Users.destroy(ctx.request.body)
  })

module.exports = router
