/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Archives
*/

const archives = require('@store/archives')
const router = require('koa-router')()
const helpers = require('@utilities/helpers')

const Joi = require('joi')
const _get = require('lodash/get')
const imageExtensions = require('image-extensions')

// middlewarese
const authentication = require('@middleware/authentication')

router
  .prefix('/archives')

  .use(authentication())

  .get('/dashboard', async (ctx, next) => {
    try {
      ctx.body = await archives.dashboard({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('unauth', '/public-archive/publish-dates', async (ctx, next) => {
    try {
      ctx.body = await archives.publicArchivePublishDates({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .get('unauth', '/', async (ctx, next) => {
    try {
      ctx.body = await archives.index({
        ...ctx.getDefaults()
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/restore', async (ctx, next) => {
    const schema = Joi.object({
      id: Joi.number().required()
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      ctx.body = await archives.restore({
        archiveID: data.id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/', async (ctx, next) => {
    const alphaJoi = name => Joi
      .string()
      .pattern(/^[A-Za-z\s]+$/)
      .messages({
        'string.pattern.base': `${name} should contain only letters`
      })

    const schema = Joi.object({
      title: Joi.string()
        .required(),
      published_date: Joi.date()
        .required(),
      adviser: Joi.string()
        .required(),
      panelists: Joi.array()
        .items(alphaJoi('Panelist'))
        .required(),
      authors: Joi.array()
        .items(Joi.object({
          name: alphaJoi('Author'),
          section: Joi.string(),
          year: Joi.string()
        }))
        .required(),
      tags: Joi.array().items(Joi.string())
        .required(),
      file: Joi.object()
        .required(),
      note: Joi.object()
        .required()
    })

    try {
      const data = await schema.validateAsync({
        title: ctx.request.body.title,
        published_date: ctx.request.body.published_date,
        adviser: ctx.request.body.adviser,
        panelists: JSON.parse(ctx.request.body.panelists),
        authors: JSON.parse(ctx.request.body.authors),
        tags: JSON.parse(ctx.request.body.tags),
        file: _get(ctx.request, 'files.file'),
        note: _get(ctx.request, 'files.note')
      })

      const fileExtension = helpers.fileExtension(data.file.name)
      const noteExtension = helpers.fileExtension(data.note.name)

      if (fileExtension !== 'pdf') {
        ctx.throw(422, 'File must be in PDF format')
      }

      if (!imageExtensions.includes(noteExtension)) {
        ctx.throw(422, 'Abstract must be an image')
      }

      const fileName = await helpers.copyFile({ file: data.file })
      const noteName = await helpers.copyFile({ file: data.note })

      const response = await archives.store({
        title: data.title,
        publishedDate: data.published_date,
        adviser: data.adviser,
        panelists: data.panelists,
        authors: data.authors,
        tags: data.tags,
        file: fileName,
        note: noteName
      })

      ctx.body = response
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  })

  .get('unauth', '/recent', async (ctx, next) => {
    ctx.body = await archives.recentList(ctx.params.id)
  })

  .get('unauth', '/popular', async (ctx, next) => {
    ctx.body = await archives.popularList(ctx.params.id)
  })

  .get('unauth', '/:id(\\d+)', async (ctx, next) => {
    try {
      ctx.body = await archives.show({
        id: ctx.params.id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .put('/', async (ctx, next) => {
    const alphaJoi = name => Joi
      .string()
      .pattern(/^[A-Za-z\s]+$/)
      .messages({
        'string.pattern.base': `${name} should contain only letters`
      })

    const schema = Joi.object({
      id: Joi.number()
        .required(),
      title: Joi.string()
        .required(),
      published_date: Joi.date()
        .required(),
      adviser: Joi.string()
        .required(),
      panelists: Joi.array()
        .items(alphaJoi('Panelist'))
        .required(),
      authors: Joi.array()
        .items(Joi.object({
          name: alphaJoi('Author'),
          section: Joi.string(),
          year: Joi.string()
        }))
        .required(),
      tags: Joi.array().items(Joi.string())
        .required(),
      file: Joi.object()
        .optional(),
      note: Joi.object()
        .optional()
    })

    try {
      const data = await schema.validateAsync({
        id: ctx.request.body.id,
        title: ctx.request.body.title,
        published_date: ctx.request.body.published_date,
        adviser: ctx.request.body.adviser,
        panelists: JSON.parse(ctx.request.body.panelists),
        authors: JSON.parse(ctx.request.body.authors),
        tags: JSON.parse(ctx.request.body.tags),
        file: _get(ctx.request, 'files.file'),
        note: _get(ctx.request, 'files.note')
      })

      let file
      if (data.file) {
        const fileExtension = helpers.fileExtension(data.file.name)
        if (fileExtension !== 'pdf') {
          ctx.throw(422, 'File must be in PDF format')
        }

        file = await helpers.copyFile({ file: data.file })
      }

      let note
      if (data.file) {
        const noteExtension = helpers.fileExtension(data.note.name)
        if (!imageExtensions.includes(noteExtension)) {
          ctx.throw(422, 'File must be in PDF format')
        }

        note = await helpers.copyFile({ file: data.note })
      }

      ctx.body = await archives.update(data.id, {
        title: data.title,
        published_date: data.published_date,
        adviser: data.adviser,
        panelists: JSON.stringify(data.panelists),
        authors: JSON.stringify(data.authors),
        tags: JSON.stringify(data.tags),
        file: file,
        note: note
      })
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  })

  .delete('/', async (ctx, next) => {
    console.log(ctx.request.body)
    const schema = Joi.object({
      id: Joi.number().required()
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      ctx.body = await archives.destroy({
        archiveID: data.id
      })
    } catch (error) {
      throw new Error(error)
    }
  })

module.exports = router
