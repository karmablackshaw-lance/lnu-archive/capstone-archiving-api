/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  Subjects
*/

const backup = require('@store/backup')
const router = require('koa-router')()
const Joi = require('joi')

router
  .prefix('/backup')

  .get('/', async (ctx, next) => {
    ctx.body = await backup.index({
      ...ctx.getDefaults()
    })
  })

  .post('/', async (ctx, next) => {
    const schema = Joi.object({
      definition: Joi.string().required()
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      ctx.body = await backup.store({
        definition: data.definition
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/restore/:backup_id', async (ctx, next) => {
    const schema = Joi.object({
      backup_id: Joi.number().required()
    })

    try {
      const data = await schema.validateAsync(ctx.params)

      ctx.body = await backup.restore({
        backupID: data.backup_id
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

  .delete('/', async (ctx, next) => {
    const schema = Joi.object({
      backupID: Joi.number().required()
    })

    try {
      const data = await schema.validateAsync(ctx.request.body)

      ctx.body = await backup.delete({
        backupID: data.backupID
      })
    } catch (error) {
      ctx.throw(error)
    }
  })

module.exports = router
