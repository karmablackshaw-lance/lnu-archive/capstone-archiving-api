const socketPort = process.env.SOCKET_PORT || 4001
const io = require('socket.io')(socketPort, { transports: ['websocket', 'polling'] })
const redis = require('@utilities/redis')

const generateSocket = name => {
  const list = {}
  list[name] = class {
    constructor () {
      this.conn = io.of(`/${name}`)
      this.conn.on('connection', () => ({}))
    }

    emit (event, message) {
      this.conn.emit(event, message)
    }
  }

  return new list[name]()
}

// ==== NAMESPACES ====
const namespaces = {
  settings: generateSocket('settings')
}

module.exports = async () => {
  const sub = redis.getNewSubscriber()

  sub.psubscribe('socket:user_events:*')
  sub.on('pmessage', (_, channel, message) => {
    const chan = channel.split(':')
    if (chan.length < 4) {
      return
    }

    if (chan[2] && namespaces[chan[2]]) {
      namespaces[chan[2]].emit(chan[3], message)
    }
  })
}
