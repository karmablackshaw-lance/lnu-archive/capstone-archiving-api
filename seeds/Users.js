const bcrypt = require('../utilities/bcrypt')
const buffer = require('../utilities/buffer')

exports.seed = async knex => {
  await knex('users').truncate()

  await knex('users').insert({
    fname: 'Karma',
    mname: 'Morningstar',
    lname: 'Blackshaw',
    level: 'admin',
    username: 'admin',
    password: await bcrypt.hash('admin'),
    temp: buffer.toBase64('admin'),
    status: 'confirmed'
  })
}
