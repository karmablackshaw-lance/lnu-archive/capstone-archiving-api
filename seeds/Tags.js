const list = [
  {
    name: 'Javascript',
    definition: 'For questions regarding programming in ECMAScript (JavaScript/JS) and its various dialects/implementations (excluding ActionScript). Please include all relevant tags on your question; e.g., [node.js], [jquery], [json], etc.'
  },
  {
    name: 'Java',
    definition: "Java is a high-level programming language. Use this tag when you're having problems using or understanding the language itself. This tag is rarely used alone and is most often used in conjunction with [spring], [spring-boot], [jakarta-ee], [android], [javafx], [hadoop], [gradle] and [maven]."
  },
  {
    name: 'C',
    definition: "C# (pronounced 'see sharp) is a high level, statically typed, multi-paradigm programming language developed \r\nby Microsoft. C# code usually targets Microsoft's .NET family of tools and run-times, which include the .NET Framework, .NET Core and Xamarin among others. Use this tag for questions about code written in C# or C#'s formal specification."
  },
  {
    name: 'Php',
    definition: 'PHP is a widely used, high-level, dynamic, object-oriented, and interpreted scripting language primarily designed for server-side web development. Used for questions about the PHP language.'
  },
  {
    name: 'Html',
    definition: "HTML (HyperText Markup Language) is the markup language for creating web pages and other information to be displayed in a web browser. Questions regarding HTML should include a minimal reproducible example and some idea of what you're trying to achieve. This tag is rarely used alone and is often paired with [CSS] and [javascript]."
  },
  {
    name: 'Ios',
    definition: 'iOS is the mobile operating system running on the Apple iPhone, iPod touch, and iPad. Use this tag [ios] for questions related to programming on the iOS platform. Use the related tags [objective-c] and [swift] for issues specific to those programming languages.'
  },
  {
    name: 'Mysql',
    definition: 'MySQL \r\nis a free, open source Relational Database Management System (RDBMS) that uses Structured Query Language (SQL). DO NOT USE this tag for other DBs such as SQL Server, SQLite etc. Those are different DBs which all use their own dialects of SQL to manage the data.'
  },
  {
    name: 'Node Js',
    definition: "Node.js is an event-based, non-blocking, asynchronous I/O runtime that uses Google's V8 JavaScript engine and libuv library. It is used \r\nfor developing applications that make heavy use of the ability to run JavaScript both on the client, as well as on server side and therefore benefit from the re-usability of code and the lack of context switching."
  },
  {
    name: 'Json',
    definition: 'JSON (JavaScript Object Notation) is a serializable data interchange format intended to be machine and human readable. Do not use this tag for native JavaScript objects or JavaScript object literals. Before you ask a question, validate your JSON using a JSON validator such as JSONLint (https://jsonlint.com).'
  },
  {
    name: 'Reactjs',
    definition: 'React is a JavaScript library for building user interfaces. It uses a declarative, component-based paradigm and aims to be both efficient and flexible.'
  },
  {
    name: 'Angularjs',
    definition: 'Use for questions about AngularJS (1.x), the open-source JavaScript framework. Do NOT use this tag for Angular 2 or later versions; instead, use the [angular] tag.'
  },
  {
    name: 'Django',
    definition: 'Django is an open-source server-side web application framework written in Python. It is designed to reduce the effort required to create complex data-driven websites and web applications, with a special focus on less code, no-redundancy and being more explicit than implicit.'
  },
  {
    name: 'Angular',
    definition: 'Questions about Angular (not to be confused with AngularJS), the web framework from Google. Use this tag for Angular questions which are not specific to an \r\nindividual version. For the older AngularJS (1.x) web framework, use the angularjs tag.'
  },
  {
    name: 'Excel',
    definition: 'Only for questions on programming against Excel objects or files, or complex formula development. You may combine the Excel tag with VBA, VSTO, C#, VB.NET, PowerShell, \r\nOLE automation, and other programming related tags and questions if applicable.'
  },
  {
    name: 'Regex',
    definition: 'Regular expressions provide a declarative language to match patterns within strings. They are commonly used for string validation, parsing, and transformation. Because regular \r\nexpressions are not fully standardized, all questions with this tag should also include a tag specifying the applicable programming language or \r\ntool.'
  },
  {
    name: 'Ruby',
    definition: 'Ruby is a multi-platform open-source, dynamic object-oriented interpreted language. The [ruby] tag is for questions related to the Ruby language, including its syntax and its libraries. Ruby on Rails questions should be tagged with [ruby-on-rails].'
  },
  {
    name: 'Pandas',
    definition: 'Pandas is a Python library for data manipulation and analysis, e.g. dataframes, multidimensional time series and \r\ncross-sectional datasets commonly found in statistics, experimental science results, econometrics, or finance. Pandas is one of the main data science libraries in Python.'
  },
  {
    name: 'Spring',
    definition: 'The Spring Framework is an open source framework for application development on the \r\nJava platform. At its core is rich support for component-based architectures, and it currently has over twenty highly integrated modules.'
  },
  {
    name: 'Laravel',
    definition: 'Laravel is a free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern and based on Symfony. The source code of Laravel is hosted on GitHub and licensed under the terms of MIT License.'
  },
  {
    name: 'String',
    definition: 'A string is a finite sequence of symbols, commonly used for text, though sometimes for arbitrary data.'
  },
  {
    name: 'Wpf',
    definition: 'Windows Presentation Foundation, or WPF, is a subsystem for rendering user interfaces in Windows-based applications.'
  },
  {
    name: 'Xcode',
    definition: "Xcode is Apple's integrated development environment (IDE). USAGE NOTE: \r\nUse this tag only for questions about the Xcode IDE itself, and not for general Mac or iOS programming topics. Use [cocoa] for Mac programming questions, and [cocoa-touch] or [iOS] or [Swift] for iOS programming questions."
  },
  {
    name: 'Windows',
    definition: 'Writing software specific to the Microsoft Windows operating system: APIs, behaviors, etc. GENERAL WINDOWS SUPPORT IS OFF-TOPIC. Support questions may be asked on https://superuser.com'
  },
  {
    name: 'Mongodb',
    definition: 'MongoDB is a scalable, high-performance, open source, document-oriented NoSQL database. It supports \r\na large number of languages and application development platforms. Questions about server administration can be asked on https://dba.stackexchange.com.'
  },
  {
    name: 'Typescript',
    definition: 'TypeScript is a typed superset of JavaScript that compiles to plain JavaScript. It adds optional types, classes, interfaces, and modules to JavaScript. This tag is for questions specific to TypeScript. It is not used for general JavaScript questions.'
  },
  {
    name: 'Bash',
    definition: 'For questions about scripts written for the Bash command shell. For shell scripts with errors/syntax errors, please check them with the shellcheck program (or in the web shellcheck server at https://shellcheck.net) beforde posting here. Questions about interactive use of Bash are more likely to be on-topic on Super User than on Stack Overflow.'
  },
  {
    name: 'Postgresql',
    definition: 'PostgreSQL is an open-source, object-relational database management system (ORDBMS) available for all major platforms including Linux, UNIX, Windows and OS X. Please mention your exact version of Postgres when asking questions. Questions concerning administration or advanced features are best directed to dba.stackexchange.com.'
  },
  {
    name: 'Oracle',
    definition: 'Oracle Database is a Multi-Model Database Management System created by Oracle Corporation. Do NOT use this tag for other products owned by Oracle, such as Java and MySQL.'
  },
  {
    name: 'Git',
    definition: 'Git is an open-source distributed version control system (DVCS). Use this tag for questions related to Git usage and workflows. DO NOT USE the [github] tag for Git-related issues simply because a repository happens to be hosted on GitHub. Also, do not use this tag for general programming questions that happen \r\nto involve a Git repository.'
  },
  {
    name: 'List',
    definition: 'The list tag may refer to: a linked list (an ordered set of nodes, each referencing \r\nits successor), or a form of dynamic array. Not to be used for HTML lists, use [html-lists] instead.'
  },
  {
    name: 'Firebase',
    definition: 'Firebase is a serverless platform for unified development of applications for mobile devices and for the web.'
  },
  {
    name: 'Algorithm',
    definition: 'An algorithm is a sequence of well-defined steps that defines an abstract solution to a problem. Use this tag when your issue is related to algorithm \r\ndesign.'
  },
  {
    name: 'Forms',
    definition: 'A form is essentially a container that can be used to hold any amount of any subset of several types of \r\ndata. HTML forms are used to pass data to a server. VB and C# forms are the windows used to interact with the user.'
  },
  {
    name: 'Azure',
    definition: 'Microsoft Azure is a Platform as a Service and Infrastructure as a Service cloud computing platform. Use this tag for programming questions \r\nconcerning Azure. General server help can be obtained at Super User or Server Fault.'
  },
  {
    name: 'Performance',
    definition: 'For questions pertaining to the measurement or improvement of code and application efficiency.'
  },
  {
    name: 'Winforms',
    definition: 'WinForms is the informal name given to Windows Forms, a GUI class library in the Microsoft .NET Framework and Mono. Questions in this tag should also be tagged with the target framework ([.net] or [mono]) and should ordinarily be tagged with a programming language tag.'
  },
  {
    name: 'Function',
    definition: 'A function (also called a procedure, method, subroutine, or routine) is a portion of code intended to carry out a single, specific task. Use this tag for questions which specifically involve creating or calling functions. For help implementing a function to perform a task, use [algorithm] or a task-specific tag instead.'
  },
  {
    name: 'Docker',
    definition: 'Docker is a tool to build and run containers. Questions concerning Dockerfiles, operations, and architecture are accepted. Questions about running docker in production may find better responses on ServerFault (https://serverfault.com/). The docker tag is rarely used alone and is often paired with other tags such as docker-compose and kubernetes.'
  },
  {
    name: 'Apache',
    definition: 'Use this tag (along with an appropriate programming-language tag) for programming questions relating to the Apache HTTP Server. Do not use this tag for questions about other Apache Foundation products. Note that server configuration questions are usually a better fit on https://serverfault.com'
  },
  {
    name: 'Api',
    definition: 'DO NOT USE. Use specific tags like [google-cloud-platform], [facebook], [amazon-web-services] instead or [api-design] where applicable. Questions asking to recommend or find an API are off-topic.'
  },
  {
    name: 'Sqlite',
    definition: 'SQLite is a software library that implements a self-contained, serverless, zero-configuration, transactional SQL database engine.'
  },
  {
    name: 'Hibernate',
    definition: 'Hibernate is an object-relational mapping (ORM) library for the Java language enabling developers to utilize POJO-style domain models in their applications in ways extending well beyond Object/Relational Mapping.'
  },
  {
    name: 'Numpy',
    definition: 'NumPy is an extension of the Python language \r\nthat adds support to large multidimensional arrays and matrixes, along with a large library of high-level mathematical functions for operations \r\nwith these arrays.'
  },
  {
    name: 'Selenium',
    definition: 'Selenium is a popular open-source tool for automating web browsers. When using this tag, also include other tags for the specific components you are using, e.g. selenium-webdriver for the language bindings, selenium-ide, selenium-grid, etc.'
  },
  {
    name: 'Rest',
    definition: 'REST (Representational State Transfer) is a style of software architecture for distributed hypermedia systems such as the World Wide Web. It has increased in popularity relative to RPC architectures such as SOAP due to the intrinsic de-coupling of client from server that comes from having a uniform interface between heterogeneous systems.'
  },
  {
    name: 'Linq',
    definition: 'Language Integrated Query (LINQ) is a Microsoft .NET Framework component that adds native data querying capabilities to .NET languages. Please consider using more detailed tags when appropriate, for example [linq-to-sql], [linq-to-entities] / [entity-framework], or [plinq]'
  },
  {
    name: 'Loops',
    definition: 'Loops are a type of control flow structure in programming in which a series of statements may be executed repeatedly until some condition is met.'
  },
  {
    name: 'Qt',
    definition: 'Qt is a cross-platform application development framework widely used for the development of application software that can be run on various software and hardware platforms with little or no change in the underlying codebase, while having the power and speed of \r\nnative applications. Qt is available with both commercial and open source licenses.'
  },
  {
    name: 'Maven',
    definition: "Apache Maven is a build automation and project management tool used primarily for Java projects. This tag is for questions that don't relate to a specific Maven version. Use the gradle tag instead for questions relating to Gradle."
  },
  {
    name: 'Swing',
    definition: 'Swing is the primary user-interface toolkit in Java and is shipped with the standard Java SDK. It is contained within the package javax.swing.'
  },
  {
    name: 'Flutter',
    definition: 'Flutter is an open-source UI software development kit created by Google. It is used to develop applications for Android, iOS, Linux, Mac, Windows, Google Fuchsia and the web from a single codebase. Flutter apps are written in the Dart language.'
  },
  {
    name: 'File',
    definition: 'A block of arbitrary information, or resource for storing information, accessible by the string-based name or path. Files are available to computer programs and are usually based on some kind of persistent storage.'
  },
  {
    name: 'Express',
    definition: 'Express is a minimal and flexible Node.js web application framework providing a robust set of features for building web applications.'
  },
  {
    name: 'Vue Js',
    definition: 'Vue.js is an open-source, progressive JavaScript framework for building user interfaces that aims to be incrementally adoptable. Vue.js is mainly used for front-end development and requires an intermediate level of HTML and CSS. Vue.js version specific questions should be tagged with [vuejs2] or [vuejs3].'
  },
  {
    name: 'Class',
    definition: 'A template for creating new objects that describes the common state(s) and behavior(s). NOT TO BE CONFUSED WITH CSS CLASSES. Use [css] instead.'
  },
  {
    name: 'Htaccess',
    definition: 'Directory-level configuration file used by Apache web servers. Use this tag if and only if .htaccess content is directly involved in the topic. We know many people are using .htaccess, but kindly ask the members of the community to not use this tag, \r\nunless you know it is on-topic in your question.'
  },
  {
    name: 'Date',
    definition: 'A date is a reference to a particular day represented within a calendar system, and consists of year, month and day.'
  },
  {
    name: 'Sorting',
    definition: 'Sorting is the process of applying some order to a collection of items.'
  },
  {
    name: 'Uitableview',
    definition: 'UITableView is a class used for displaying and editing lists of information on iOS. A table view displays items in a single column. UITableView is a subclass of UIScrollView, which allows users to scroll through the table, although UITableView allows vertical scrolling only.'
  }
]

exports.seed = async knex => {
  await knex.raw('SET FOREIGN_KEY_CHECKS=0')
  await knex('tags').truncate()
  await knex('tags').insert(list)
  await knex.raw('SET FOREIGN_KEY_CHECKS=1')
}
