/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Archives
*/

const { knex, jsonObject, raw, makeQuery } = require('@utilities/knex')

const _omitBy = require('lodash/omitBy')
const _isNil = require('lodash/isNil')
const _size = require('lodash/size')
const _pick = require('lodash/pick')

module.exports = {
  async index ({ filterBy, q, dataStatus }) {
    const dictionary = {
      keywords: 'archives.tags',
      author: 'archives.authors',
      archive: ['archives.title', 'archives.note'],
      title: 'archives.title',
      year: raw('YEAR(archives.published_date)')
    }

    const list = await knex('archives')
      .orderBy('archives.id', 'desc')
      .select({
        id: 'archives.id',
        authors: 'archives.authors',
        adviser: 'archives.adviser',
        panelists: 'archives.panelists',
        tags: 'archives.tags',
        title: 'archives.title',
        note: 'archives.note',
        path: 'archives.path',
        published_date: 'archives.published_date',
        created_at: 'archives.created_at',
        deleted_at: 'archives.deleted_at'
      })
      .modify(knex => {
        makeQuery({
        // ...{ sortBy: sortByColumn, sort },
        // ...{ dateBy: dateByColumn, dateFrom, dateTo },
          ...{ filterBy, q, dictionary },
          knex
        // isCount
        })

        if (filterBy === 'all') {
          const allCols = ['archives.authors', 'archives.title', 'archives.note', raw('YEAR(archives.published_date)')]

          knex.where(function () {
            for (let i = 0; i < allCols.length; i++) {
              const curr = allCols[i]

              this.orWhere(curr, 'like', `%${q}%`)
            }
          })
        }

        if (dataStatus === 'deleted') {
          knex.whereNotNull('archives.deleted_at')
        } else {
          knex.whereNull('archives.deleted_at')
        }
      })

    for (let i = 0; i < list.length; i++) {
      const curr = list[i]
      curr.authors = JSON.parse(curr.authors)
      curr.tags = JSON.parse(curr.tags)
      curr.panelists = JSON.parse(curr.panelists)
    }

    return list
  },

  async store ({ title, publishedDate, adviser, panelists, authors, tags, file, note }) {
    try {
      const [archiveID] = await knex('archives')
        .insert({
          title,
          adviser,
          panelists: JSON.stringify(panelists),
          tags: JSON.stringify(tags),
          published_date: publishedDate,
          authors: JSON.stringify(authors),
          path: '/uploads/' + file,
          note: '/uploads/' + note
        })

      return archiveID
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  },

  async recentList () {
    const archivesJsonObject = jsonObject({
      id: 'archives.id',
      title: 'archives.title',
      note: 'archives.note',
      published_date: 'archives.published_date',
      created_at: 'archives.created_at'
    }, 1)

    const list = await knex('archives')
      .groupBy('archives.id')
      .orderBy('archives.created_at', 'desc')
      .whereNull('archives.deleted_at')
      .limit(10)
      .select({
        authors: 'archives.authors',
        adviser: 'archives.adviser',
        panelists: 'archives.panelists',
        tags: 'archives.tags',
        archives: raw(archivesJsonObject)
      })

    for (let i = 0; i < list.length; i++) {
      const curr = list[i]
      curr.authors = JSON.parse(curr.authors)
      curr.archives = JSON.parse(curr.archives)
      curr.tags = JSON.parse(curr.tags)
      curr.panelists = JSON.parse(curr.panelists)
    }

    return list
  },

  async popularList () {
    const authorJsonObject = jsonObject({
      id: 'authors.id',
      name: 'authors.name'
    })

    const archivesJsonObject = jsonObject({
      id: 'archives.id',
      title: 'archives.title',
      note: 'archives.note',
      published_date: 'archives.published_date',
      created_at: 'archives.created_at'
    }, 1)

    const tagsJsonObject = jsonObject({
      id: 'tags.id',
      name: 'tags.name'
    })

    const list = await knex('archives')
      .join({ aauthors: 'archive_authors' }, 'aauthors.archive_id', 'archives.id')
      .join({ authors: 'authors' }, 'authors.id', 'aauthors.author_id')
      .join({ aatags: 'archive_tags' }, 'aatags.archive_id', 'archives.id')
      .join({ tags: 'tags' }, 'tags.id', 'aatags.tag_id')
      .groupBy('archives.id')
      .orderBy('archives.created_at', 'desc')
      .limit(10)
      .whereNull('archives.deleted_at')
      .whereNull('aauthors.deleted_at')
      .whereNull('authors.deleted_at')
      .whereNull('aatags.deleted_at')
      .whereNull('tags.deleted_at')
      .select({
        authors: raw(`CONCAT('[', GROUP_CONCAT(DISTINCT(${authorJsonObject})), ']')`),
        archives: raw(archivesJsonObject),
        tags: raw(`CONCAT('[', GROUP_CONCAT(DISTINCT(${tagsJsonObject})), ']')`)
      })

    for (let i = 0; i < list.length; i++) {
      const curr = list[i]
      curr.authors = JSON.parse(curr.authors)
      curr.archives = JSON.parse(curr.archives)
      curr.tags = JSON.parse(curr.tags)
    }

    return list
  },

  async publicArchivePublishDates () {
    try {
      const list = await knex('archives')
        .groupByRaw('DATE(published_date)')
        .orderBy('count', 'desc')
        .whereNull('archives.deleted_at')
        .select({
          date: raw('DATE(published_date)'),
          count: raw('COUNT(archives.id)')
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async dashboard () {
    try {
      const list = await knex('archives')
        .leftJoin({ atags: 'archive_tags' }, 'atags.archive_id', 'archives.id')
        .leftJoin({ tags: 'tags' }, 'tags.id', 'atags.tag_id')
        .groupBy('archives.id')
        .whereNull('archives.deleted_at')
        .whereNull('atags.deleted_at')
        .whereNull('tags.deleted_at')
        .select({ total: raw('COUNT(archives.id) OVER ()') })
        .first()

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async show ({ id }) {
    try {
      const data = await knex('archives')
        .where({ id })
        .whereNull('archives.deleted_at')
        .first()
        .select({
          id: 'archives.id',
          title: 'archives.title',
          note: 'archives.note',
          tags: 'archives.tags',
          authors: 'archives.authors',
          path: 'archives.path',
          panelists: 'archives.panelists',
          adviser: 'archives.adviser',
          published_date: 'archives.published_date',
          created_at: 'archives.created_at'
        })

      if (!data) {
        return
      }

      data.authors = JSON.parse(data.authors)
      data.tags = JSON.parse(data.tags)
      data.panelists = JSON.parse(data.panelists)

      return data
    } catch (error) {
      throw error
    }
  },

  async update (id, payload) {
    try {
      const columns = [
        'title',
        'published_date',
        'adviser',
        'panelists',
        'authors',
        'tags',
        'file',
        'note'
      ]

      const data = _omitBy(_pick(payload, columns), _isNil)

      if (!_size(data)) {
        return
      }

      await knex('archives')
        .where('id', id)
        .update(data)

      return data
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  },

  restore ({ archiveID }) {
    return knex('archives')
      .where('archives.id', archiveID)
      .update({ 'archives.deleted_at': null })
  },

  destroy ({ archiveID }) {
    return knex('archives')
      .where('archives.id', archiveID)
      .update({ 'archives.deleted_at': new Date() })
  }
}
