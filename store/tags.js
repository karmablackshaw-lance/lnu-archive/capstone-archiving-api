/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Tags
*/

const { knex, makeQuery, raw } = require('@utilities/knex')

module.exports = {
  async dashboard () {
    try {
      const list = await knex('tags')
        .whereNull('tags.deleted_at')
        .select({ total: raw('COUNT(tags.id)') })
        .first()

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async index ({ filterBy, q, dataStatus }) {
    const dictionary = {
      name: 'tags.name',
      definition: 'tags.definition'
    }

    const list = await knex('tags')
      .select({
        tag_id: 'tags.id',
        name: 'tags.name',
        definition: 'tags.definition',
        deleted_at: 'tags.deleted_at'
      })
      .orderBy('tags.id', 'desc')
      .modify(knex => {
        makeQuery({
        // ...{ sortBy: sortByColumn, sort },
        // ...{ dateBy: dateByColumn, dateFrom, dateTo },
          ...{ filterBy, q, dictionary },
          knex
        // isCount
        })

        if (dataStatus === 'deleted') {
          knex.whereNotNull('tags.deleted_at')
        } else {
          knex.whereNull('tags.deleted_at')
        }
      })

    return list
  },

  async store ({ tag, definition }) {
    try {
      const find = await this.findTagBy('name', tag)

      if (find) {
        throw new Error('Tag exists')
      }

      return knex('tags').insert({
        name: tag,
        definition
      })
    } catch (error) {
      throw new Error(error)
    }
  },

  async findTagBy (filterBy, q) {
    return knex('tags')
      .where(filterBy, q)
      .first()
  },

  async show (id) {
    return 'show'
  },

  update ({ tagID, tag, definition }) {
    return knex('tags')
      .where('id', tagID)
      .update({
        name: tag,
        definition
      })
  },

  async destroy (id) {
    return 'destroy'
  },

  async publicArchive () {
    try {
      const list = await knex('tags')
        .leftJoin({ atags: 'archive_tags' }, 'atags.tag_id', 'tags.id')
        .whereNull('tags.deleted_at')
        .whereNull('atags.deleted_at')
        .groupBy('atags.archive_id')
        .groupBy('tags.id')
        .orderBy('count', 'desc')
        .select({
          name: 'tags.name',
          tag_id: 'tags.id',
          count: raw('COUNT(atags.id)')
        })

      return list
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  },

  async homeList () {
    const list = await knex('tags')
      .leftJoin({ atags: 'archive_tags' }, 'atags.tag_id', 'tags.id')
      .whereNull('tags.deleted_at')
      .whereNull('atags.deleted_at')
      .groupBy('tags.id')
      .select({
        tag_id: 'tags.id',
        tag: 'tags.name',
        count: raw('COUNT(atags.id)')
      })
      .orderBy('count', 'desc')
      .limit(100)

    return list
  },

  deleteTag ({ tagID }) {
    return knex('tags')
      .update('deleted_at', new Date())
      .where('id', tagID)
  },

  restoreTag ({ tagID }) {
    return knex('tags')
      .update('deleted_at', null)
      .where('id', tagID)
  }
}
