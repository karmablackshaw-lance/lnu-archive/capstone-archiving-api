/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Auth
*/

const { knex } = require('@utilities/knex')
const bcrypt = require('@utilities/bcrypt')
const jwt = require('@utilities/jwt')

module.exports = {
  async index (params) {
    return 'index'
  },

  async store (data) {
    return 'store'
  },

  async login ({ username, password }) {
    try {
      const userData = await knex('users')
        .whereNull('users.deleted_at')
        .where('username', username)
        .first()
        .select({
          id: 'users.id',
          fname: 'users.fname',
          mname: 'users.mname',
          lname: 'users.lname',
          level: 'users.level',
          status: 'users.status',
          color: 'users.color',
          password: 'users.password',
          created_at: 'users.created_at'
        })

      if (!userData) {
        throw new Error('Invalid username or password')
      }

      if (userData.level === 'student' && userData.status !== 'confirmed') {
        throw new Error('Account still pending')
      }

      const verification = await bcrypt.verify({
        password: password,
        hash: userData.password
      })

      if (!verification) {
        throw new Error('Invalid username or password')
      }

      const token = await jwt.sign({
        id: userData.id,
        fname: userData.fname,
        mname: userData.mname,
        lname: userData.lname,
        level: userData.level,
        acronym: String(userData.fname[0] + userData.mname[0]).toUpperCase(),
        color: userData.color,
        created_at: userData.created_at
      })

      return token
    } catch (error) {
      console.log(error)
      throw new Error(error)
    }
  },

  async show (id) {
    return 'show'
  },

  async update (id) {
    return 'update'
  },

  async destroy (id) {
    return 'destroy'
  }
}
