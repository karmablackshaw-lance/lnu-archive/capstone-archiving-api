/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Archives
*/

const Promise = require('bluebird')
const fs = Promise.promisifyAll(require('fs'))
const moment = require('moment')

// utilities
const { knex } = require('@utilities/knex')

module.exports = {
  async index () {
    return knex('backups')
      .whereNull('backups.deleted_at')
      .orderBy('backups.created_at', 'desc')
  },

  async store ({ definition }) {
    try {
      const tables = [
        'archives',
        'archive_tags',
        'archives',
        'tags',
        'users'
      ]

      const queries = []
      for (let i = 0; i < tables.length; i++) {
        const curr = tables[i]
        queries.push(knex(curr))
      }

      const resultObject = {}
      const results = await Promise.all(queries)

      for (let i = 0; i < results.length; i++) {
        const currResult = results[i]
        const currTable = tables[i]
        resultObject[currTable] = currResult
      }

      const filename = moment().format('YYYYMMDD_HHmmss')

      await fs.writeFileAsync(`./backups/${filename}.json`, JSON.stringify(resultObject))

      await knex('backups').insert({ filename, definition })
    } catch (error) {
      throw new Error(error)
    }
  },

  async restore ({ backupID }) {
    try {
      const find = await knex('backups')
        .where('id', backupID)
        .first()

      if (!find) {
        throw new Error('Backup not found')
      }

      const findFile = await fs.readFileAsync(`./backups/${find.filename}.json`, 'utf8')
      const data = JSON.parse(findFile)

      await knex.raw('SET FOREIGN_KEY_CHECKS=0')

      for (const table in data) {
        const currData = data[table]

        await knex(table).truncate()
        await knex(table).insert(currData)
      }

      await knex.raw('SET FOREIGN_KEY_CHECKS=1')
    } catch (error) {
      throw new Error(error)
    }
  },

  delete ({ backupID }) {
    return knex('backups')
      .where('id', backupID)
      .del()
  }
}
