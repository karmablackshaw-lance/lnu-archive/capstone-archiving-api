/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Subjects
*/

const { knex, makeQuery, jsonObject, raw } = require('@utilities/knex')
const _groupBy = require('lodash/groupBy')

module.exports = {
  async dashboard () {
    try {
      const list = await knex('subjects')
        .join({ sub: 'sub_subjects' }, 'sub.subject_id', 'subjects.id')
        .whereNull('subjects.deleted_at')
        .whereNull('sub.deleted_at')
        .first()
        .select({
          total: raw('COUNT(sub.id)')
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async subSubjectsList ({ filterBy, q, dataStatus }) {
    const dictionary = {
      subject: 'subjects.subject',
      sub: 'sub.name'
    }

    const list = await knex('subjects')
      .join({ sub: 'sub_subjects' }, 'sub.subject_id', 'subjects.id')
      .orderBy('sub.id', 'desc')
      .select({
        subject: 'subjects.subject',
        subject_id: 'subjects.id',
        sub_id: 'sub.id',
        sub: 'sub.name',
        sub_deleted_at: 'sub.deleted_at',
        subject_deleted_at: 'subjects.deleted_at'
      })
      .modify(knex => {
        makeQuery({
        // ...{ sortBy: sortByColumn, sort },
        // ...{ dateBy: dateByColumn, dateFrom, dateTo },
          ...{ filterBy, q },
          knex,
          dictionary
        // isCount
        })

        if (dataStatus === 'deleted') {
          knex.where(function () {
            this.orWhereNotNull('subjects.deleted_at')
            this.orWhereNotNull('sub.deleted_at')
          })
        } else {
          knex.whereNull('subjects.deleted_at')
          knex.whereNull('sub.deleted_at')
        }
      })

    return list
  },

  async subjectsList (params) {
    try {
      const list = await knex('subjects')
        .whereNull('subjects.deleted_at')
        .select({
          subject: 'subjects.subject',
          subject_id: 'subjects.id'
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async publicArchive () {
    try {
      const list = await knex('subjects')
        .leftJoin({ sub: 'sub_subjects' }, 'sub.subject_id', 'subjects.id')
        .leftJoin({ asub: 'archive_subject' }, 'asub.sub_subject_id', 'sub.id')
        .groupBy('sub.id')
        .orderBy('count', 'desc')
        .whereNull('subjects.deleted_at')
        .whereNull('sub.deleted_at')
        .whereNull('asub.deleted_at')
        .select({
          sub_id: 'sub.id',
          name: 'sub.name',
          subject: 'subjects.subject',
          subject_id: 'subjects.id',
          count: raw('COUNT(asub.id)')
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async homeList (params) {
    const list = await knex('subjects')
      .whereNull('subjects.deleted_at')
      .groupBy('subjects.id')
      .select({
        subject: 'subjects.subject',
        subject_id: 'subjects.id'
      })

    const subList = await knex({ sub: 'sub_subjects' })
      .whereNull('sub.deleted_at')
      .select({
        id: 'sub.id',
        name: 'sub.name',
        subject_id: 'sub.subject_id'
      })

    const subListBySubject = _groupBy(subList, 'subject_id')

    for (let i = 0; i < list.length; i++) {
      const curr = list[i]
      curr.subs = subListBySubject[curr.subject_id]
    }

    return list
  },

  findSubjectBy (filterBy, q) {
    return knex('subjects')
      .where(filterBy, q)
      .first()
  },

  findSubSubjectBy (filterBy, q) {
    return knex('sub_subjects')
      .where(filterBy, q)
      .first()
  },

  async store ({ subject }) {
    try {
      const find = await this.findSubjectBy('subject', subject)

      if (find) {
        throw new Error('Subject already exists')
      }

      return knex('subjects').insert({ subject })
    } catch (error) {
      throw new Error(error)
    }
  },

  async createSubSubject ({ subjectID, name }) {
    try {
      const find = await this.findSubSubjectBy('name', name)

      if (find) {
        throw new Error('Sub subject already exists')
      }

      return knex('sub_subjects').insert({ subject_id: subjectID, name })
    } catch (error) {
      throw new Error(error)
    }
  },

  updateSubSubject ({ subjectID, subID, name }) {
    return knex('sub_subjects')
      .where('id', subID)
      .update({
        subject_id: subjectID,
        name
      })
  },

  async show (id) {
    return 'show'
  },

  async updateSubject ({ subjectID, subject }) {
    return knex('subjects')
      .where('id', subjectID)
      .update({ subject })
  },

  deleteSubject ({ subjectID }) {
    return knex('subjects')
      .update('deleted_at', new Date())
      .where('id', subjectID)
  },

  deleteSubSubject ({ subID }) {
    return knex('sub_subjects')
      .update('deleted_at', new Date())
      .where('id', subID)
  },

  restoreSubject ({ subjectID }) {
    return knex('subjects')
      .update('deleted_at', null)
      .where('id', subjectID)
  },

  restoreSubSubject ({ subID }) {
    return knex('sub_subjects')
      .update('deleted_at', null)
      .where('id', subID)
  }
}
