/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Users
*/

const { knex, makeQuery } = require('@utilities/knex')
const { getKey, isPOJO } = require('@utilities/helpers')
const bcrypt = require('@utilities/bcrypt')
const buffer = require('@utilities/buffer')

const raw = qb => knex.raw(qb)

const _pick = require('lodash/pick')
const _isNil = require('lodash/isNil')

module.exports = {
  async index ({ q, filterBy, sort, sortBy, dateBy, dateFrom, dateTo, isCount, status, level }) {
    try {
      const dictionary = {
        fname: 'users.fname',
        lname: 'users.lname',
        mname: 'users.mname'
      }

      const list = await knex('users')
        .whereNot('users.level', 'admin')
        .modify(knex => {
          makeQuery({
            ...{ filterBy, q },
            ...{ sortBy, sort },
            ...{ dateBy, dateFrom, dateTo },
            // ...{ page, rows },
            knex,
            dictionary,
            isCount
          })

          if (['student', 'user'].includes(level)) {
            knex.where('level', level)
          }

          if (['confirmed', 'pending', 'rejected'].includes(status)) {
            knex.where('users.status', status)
          }

          if (isCount) {
            knex.count({ total: 1 }).first()
          } else {
            knex.select({
              user_id: 'users.id',
              fname: 'users.fname',
              mname: 'users.mname',
              lname: 'users.lname',
              username: 'users.username',
              color: 'users.color',
              status: 'users.status',
              created_at: 'users.created_at'
            })
          }
        })

      if (isCount) {
        return list
      }

      return list
    } catch (error) {
      throw error
    }
  },

  findUserBy (filterBy, q) {
    const filterByColumn = getKey(filterBy, {
      user_id: 'users.id'
    })

    if (!filterByColumn) {
      throw new Error('Invalid filter')
    }

    return knex('users')
      .where(filterByColumn, q)
      .first()
      .select({
        user_id: 'users.id',
        fname: 'users.fname',
        mname: 'users.mname',
        lname: 'users.lname',
        level: 'users.level',
        username: 'users.username',
        color: 'users.color',
        acronym: raw('CONCAT(LEFT(users.fname, 1), LEFT(users.lname, 1))')
      })
  },

  async findBy (filterBy, q) {
    const dictionary = {
      fname: 'users.fname',
      mname: 'users.mname',
      lname: 'users.lname',
      username: 'users.username',
      password: 'users.password'
    }

    const filterByColumn = getKey(filterBy, dictionary)

    try {
      const data = await knex('users')
        .modify(knex => {
          if (filterByColumn && !_isNil(q)) {
            knex.where(filterByColumn, q)
          }

          if (isPOJO(filterBy)) {
            for (const key in filterBy) {
              if (dictionary[key] && !_isNil(filterBy[key])) {
                knex.where(dictionary[key], filterBy[key])
              }
            }
          }
        })
        .select(dictionary)
        .first()

      return data
    } catch (error) {
      throw new Error(error)
    }
  },

  async store ({ fname, mname, lname, username, password }) {
    try {
      const hash = await bcrypt.hash(password)
      const base64 = buffer.toBase64(password)

      return knex('users')
        .insert({
          fname,
          mname,
          lname,
          level: 'student',
          username,
          password: hash,
          temp: base64,
          status: 'pending'
        })
    } catch (error) {
      throw new Error(error)
    }
  },

  async dashboard () {
    try {
      return knex('users')
        .where('users.level', 'student')
        .select({
          total_confirmed: raw("SUM(IF(users.status = 'confirmed', 1, 0))"),
          total_pending: raw("SUM(IF(users.status = 'pending', 1, 0))")
        })
        .first()
    } catch (error) {
      throw new Error(error)
    }
  },

  async show (id) {
    return 'show'
  },

  update (data) {
    const columns = [
      'color',
      'fname',
      'level',
      'lname',
      'mname',
      'username',
      'status',
      'password',
      'temp'
    ]

    return knex('users')
      .where('id', data.user_id)
      .update(_pick(data, columns))
  },

  async destroy (id) {
    return 'destroy'
  }
}
