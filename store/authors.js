/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module Authors
*/

const { knex, makeQuery, jsonObject, raw } = require('@utilities/knex')

module.exports = {
  async dashboard () {
    try {
      const list = await knex('authors')
        .whereNull('authors.deleted_at')
        .first()
        .select({
          total: raw('COUNT(authors.id)')
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async index ({ filterBy, q, dataStatus }) {
    const dictionary = {
      name: ['authors.name', 'authors.id']
    }

    const list = await knex('authors')
      .select({
        author_id: 'authors.id',
        name: 'authors.name',
        deleted_at: 'authors.deleted_at'
      })
      .modify(knex => {
        makeQuery({
        // ...{ sortBy: sortByColumn, sort },
        // ...{ dateBy: dateByColumn, dateFrom, dateTo },
          // ...{ page, rows },
          ...{ filterBy, q, dictionary },
          knex
        // isCount
        })

        if (dataStatus === 'deleted') {
          knex.whereNotNull('authors.deleted_at')
        } else {
          knex.whereNull('authors.deleted_at')
        }
      })

    return list
  },

  async publicArchive () {
    try {
      const list = await knex('authors')
        .join({ aauthors: 'archive_authors' }, 'aauthors.author_id', 'authors.id')
        .join({ archives: 'archives' }, 'archives.id', 'aauthors.archive_id')
        .whereNull('authors.deleted_at')
        .whereNull('aauthors.deleted_at')
        .whereNull('archives.deleted_at')
        .groupBy('authors.id')
        .orderBy('count', 'desc')
        .select({
          name: 'authors.name',
          id: 'authors.id',
          count: raw('COUNT(archives.id)')
        })

      return list
    } catch (error) {
      throw new Error(error)
    }
  },

  async publishersList ({ q, filterBy, dataStatus }) {
    const dictionary = {
      archive: ['archives.title'],
      author: ['authors.name'],
      subject: ['ssubjects.name', 'subjects.subject']
    }

    const archivesJsonObject = jsonObject({
      id: 'archives.id',
      title: 'archives.title'
    })

    const subjectsJsonObject = jsonObject({
      sub_id: 'ssubjects.id',
      sub: 'ssubjects.name',
      subject_id: 'subjects.id',
      subject: 'subjects.subject'
    })

    const list = await knex('authors')
      .leftJoin({ aauthors: 'archive_authors' }, 'aauthors.author_id', 'authors.id')
      .leftJoin({ archives: 'archives' }, 'archives.id', 'aauthors.archive_id')
      .leftJoin({ asubjects: 'archive_subject' }, 'asubjects.archive_id', 'archives.id')
      .leftJoin({ ssubjects: 'sub_subjects' }, 'ssubjects.id', 'asubjects.sub_subject_id')
      .leftJoin({ subjects: 'subjects' }, 'subjects.id', 'ssubjects.subject_id')
      .groupBy('authors.id')
      .orderBy('authors.id', 'desc')
      .select({
        author_id: 'authors.id',
        name: 'authors.name',
        archives: raw(`CONCAT('[', GROUP_CONCAT(DISTINCT(${archivesJsonObject})), ']')`),
        subjects: raw(`CONCAT('[', GROUP_CONCAT(DISTINCT(${subjectsJsonObject})), ']')`),
        author_deleted_at: 'authors.deleted_at'
      })
      .modify(knex => {
        makeQuery({
        // ...{ sortBy: sortByColumn, sort },
        // ...{ dateBy: dateByColumn, dateFrom, dateTo },
          // ...{ page, rows },
          ...{ filterBy, q, dictionary },
          knex
        // isCount
        })

        if (dataStatus === 'deleted') {
          knex.whereNotNull('authors.deleted_at')
        } else {
          knex.whereNull('authors.deleted_at')
          knex.whereNull('aauthors.deleted_at')
          knex.whereNull('archives.deleted_at')
          knex.whereNull('asubjects.deleted_at')
          knex.whereNull('ssubjects.deleted_at')
          knex.whereNull('subjects.deleted_at')
        }
      })

    for (let i = 0; i < list.length; i++) {
      const curr = list[i]
      curr.archives = JSON.parse(curr.archives).filter(x => x.id)
      curr.subjects = JSON.parse(curr.subjects).filter(x => x.sub_id)
    }

    return list
  },

  async store (data) {
    try {
      const findAuthor = await this.findAuthorBy('name', data.author)

      if (findAuthor) {
        throw new Error('Author already exists')
      }

      return knex('authors').insert({
        name: data.author
      })
    } catch (error) {
      throw new Error(error)
    }
  },

  findAuthorBy (filterBy, q) {
    return knex('authors')
      .where(filterBy, q)
      .whereNull('authors.deleted_at')
      .first()
  },

  async show (id) {
    return 'show'
  },

  update ({ authorID, name }) {
    return knex('authors')
      .update({ name })
      .where('id', authorID)
  },

  deleteAuthor ({ authorID }) {
    return knex('authors')
      .update('deleted_at', new Date())
      .where('id', authorID)
  },

  restoreAuthor ({ authorID }) {
    return knex('authors')
      .update('deleted_at', null)
      .where('id', authorID)
  }

}
