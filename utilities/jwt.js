const SECRET_KEY = process.env.SECRET_KEY
const Promise = require('bluebird')
const jwt = Promise.promisifyAll(require('jsonwebtoken'))

module.exports = {
  sign (payload) {
    return jwt.signAsync({ data: payload }, SECRET_KEY)
  },

  verify (token) {
    return jwt.verifyAsync(token, SECRET_KEY)
  }
}
