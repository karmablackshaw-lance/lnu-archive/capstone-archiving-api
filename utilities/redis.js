const Promise = require('bluebird')
const redis = Promise.promisifyAll(require('redis'))
const _flatten = require('lodash/flatten')

module.exports = {
  client: null,
  subscribers: [],
  publisher: null,

  start () {
    this.client = this.getConnection()
    this.publisher = this.getConnection()
  },

  getNewSubscriber () {
    const newSubscriber = this.getConnection()
    this.subscribers.push(newSubscriber)
    return newSubscriber
  },

  getConnection (num) {
    return redis.createClient({
      host: process.env.REDIS_HOST || '127.0.0.1',
      port: process.env.REDIS_PORT || 6379,
      password: process.env.REDIS_PASSWORD || undefined,
      db: process.env.REDIS_DB || num || 0
    })
  },

  cache (key, params) {
    return this.redis.client.hmsetAsync(key, _flatten(params))
  },

  async read (key, callback) {
    let result = await this.redis.client.hgetallAsync(key)
    let cbResult = null

    if (!result) {
      cbResult = await callback()
    }

    if (cbResult) {
      this.cache(key, cbResult)
      result = cbResult
    }

    return result
  }
}
