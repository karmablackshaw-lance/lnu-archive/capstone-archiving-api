module.exports = {
  toBase64 (str) {
    return Buffer.from(str).toString('base64')
  },

  fromBase64 (b64) {
    return Buffer.from(b64, 'base64').toString()
  }
}
