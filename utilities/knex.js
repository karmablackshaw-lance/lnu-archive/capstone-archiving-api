const knexFile = require('../knexfile')[process.env.NODE_ENV || 'development']
const knex = require('knex')(knexFile)
const _isEmpty = require('lodash/isEmpty')
const raw = qb => knex.raw(qb)

const isArray = item => item && Array.isArray(item)
const isDate = date => !isNaN(new Date(date).getTime())

const knexHelper = {
  knex,

  raw,

  jsonObject (data, isRaw) {
    const cols = Object
      .keys(data)
      .reduce((acc, curr) => [...acc, `"${curr}", ${data[curr]}`], [])
      .join(', ')
      .trim()

    return isRaw
      ? raw(`JSON_OBJECT(${cols})`)
      : `JSON_OBJECT(${cols})`
  },

  jsonExtract (column, key, raw) {
    return raw
      ? knexHelper.raw(`JSON_EXTRACT(${column}, '$.${key}')`)
      : `JSON_EXTRACT(${column}, '$.${key}')`
  },

  makeQuery ({
    knex,
    filterBy,
    q,
    page,
    rows,
    sortBy,
    sort,
    dateBy,
    dateFrom,
    dateTo,
    isCount,
    dictionary
  }) {
    if (Number(page) && Number(rows) && !isCount) {
      knex.limit(rows).offset(rows * (page - 1))
    }

    if (isArray(sortBy) && isArray(sort) && dictionary) {
      for (let i = 0; i < sortBy.length; i++) {
        const currSortBy = dictionary[sortBy[i]]
        const currSort = sort[i]

        if (!currSortBy || !currSort) {
          continue
        }

        knex.orderBy(currSortBy, currSort)
      }
    }

    if (isArray(dateBy) && isArray(dateFrom) && isArray(dateTo) && dictionary) {
      knex.where(function () {
        for (let i = 0; i < dateBy.length; i++) {
          const currDateBy = dictionary[dateBy[i]]
          const currDateFrom = dateFrom[i]
          const currDateTo = dateTo[i]

          if (!isDate(currDateFrom) || !isDate(currDateTo) || !currDateBy) {
            continue
          }

          knex.orWhereBetween(currDateBy, [currDateFrom, currDateTo])
        }
      })
    }

    if (isArray(filterBy) && isArray(q) && dictionary) {
      knex.where(function () {
        for (let i = 0; i < q.length; i++) {
          const currQ = q[i]
          const currFilter = dictionary[filterBy[i]]

          if (!currQ || !currFilter) {
            continue
          }

          this.orWhere(currFilter, 'like', `%${currQ}%`)
        }
      })
    }
  },

  wrapCase (caseStatements, raw) {
    if (_isEmpty(caseStatements)) {
      throw new Error(`Case statements expected a value. Received ${caseStatements}`)
    }

    return raw
      ? knexHelper.raw(`(CASE ${caseStatements} END)`)
      : `(CASE ${caseStatements} END)`
  }
}

module.exports = knexHelper
