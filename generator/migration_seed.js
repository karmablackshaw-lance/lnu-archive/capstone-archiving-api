const chalk = require('chalk')

const d = new Date()
const date = d.getFullYear().toString().concat(d.getMonth() + 1, d.getDate(), d.getHours(), d.getMinutes(), d.getSeconds())

module.exports = plop => {
  plop.load('./utility', {}, { helpers: true })

  plop.setGenerator('module', {
    description: 'Generate migration and seed module',
    prompts: [
      {
        type: 'input',
        name: 'name',
        message: 'Generate migration and seed module:',
        validate: function (input) {
          if (isNaN(parseInt(input))) return true

          const error = [
            '-------------------------------------------------------',
            ':::::::::: :::::::::  :::::::::   ::::::::  :::::::::',
            ':+:        :+:    :+: :+:    :+: :+:    :+: :+:    :+:',
            '+:+        +:+    +:+ +:+    +:+ +:+    +:+ +:+    +:+ ',
            '+#++:++#   +#++:++#:  +#++:++#:  +#+    +:+ +#++:++#:  ',
            '+#+        +#+    +#+ +#+    +#+ +#+    +#+ +#+    +#+ ',
            '#+#        #+#    #+# #+#    #+# #+#    #+# #+#    #+# ',
            '########## ###    ### ###    ###  ########  ###    ### ',
            '-------------------------------------------------------'
          ].join('\n')

          console.log(chalk.bold.red(error))
          console.log(chalk.bold.red('Number is not a valid file name!\n'))
        }
      }
    ],
    actions: [
      {
        type: 'add',
        path: `../migrations/${date}_{{pascalCase name}}.js`,
        templateFile: './knex/migrationTemplate.js',
        skipIfExists: true
      },
      {
        type: 'add',
        path: '../seeds/{{pascalCase name}}.js',
        templateFile: './knex/seedTemplate.js',
        skipIfExists: true
      }
    ]
  })
}
