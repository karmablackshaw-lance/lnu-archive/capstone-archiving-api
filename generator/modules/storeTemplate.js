/**
 * TODO - Describe what this store does in general.
 * CRUD Operations are done here...
 * @module {{pascalCase name}}
*/

const { knex, makeQuery } = require('@utilities/knex')

module.exports = {
  async index ({ filterBy, q, page, rows, sortBy, sort, dateBy, dateFrom, dateTo, isCount, dataStatus }) {
    const dictionary = {
      name: 'sample.name'
    }

    const list = await knex('sample')
      .modify(knex => {
        makeQuery({
          ...{ sortBy, sort },
          ...{ page, rows },
          ...{ dateBy, dateFrom, dateTo },
          ...{ filterBy, q },
          dictionary,
          isCount,
          knex
        })

        if (isCount) {
          knex.count({ total: 1 }).first()
        } else {
          knex.select({
            id: 'sample.id'
          })
        }
      })

    return list
  },

  async store (data) {
    return 'store'
  },

  async show (id) {
    return 'show'
  },

  async update (id) {
    return 'update'
  },

  async destroy (id) {
    return 'destroy'
  }
}
