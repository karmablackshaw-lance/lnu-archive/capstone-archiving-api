/**
 * TODO - Describe what this test does in general.
 * @Test Module {{pascalCase name}}
*/

const {{ camelCase name }} = require(`../store/{{ kebabCase name }}`)
const assert = require('assert')
const faker = require('faker')

describe(`{{ pascalCase name }}`, function() {

  /**
   * Description - Get all `{{ lowerCase name }}`
   * @action  '{{ kebabCase name }}/'
   */
  describe('index', function() {
    it(`should return {{ pascalCase name }}`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Get all `{{ lowerCase name }}`
   * @action  '{{ kebabCase name }}/'
   */
  describe('store', function() {
    it(`should store {{ pascalCase name }}`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Get specific `{{ kebabCase name }}
   * @action  '{{ kebabCase name }}/:{{ kebabCase name }}_id'
   */
  describe('show', function() {
    it(`should show {{ pascalCase name }}`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Update `{{ kebabCase name }}`
   * @action  '{{ kebabCase name }}/:{{ kebabCase name }}_id'
   */
  describe('update', function() {
    it(`should update {{ pascalCase name }}`, function() {
      assert.equal(true, true)
    })
  })

  /**
   * Description - Remove  `{{ kebabCase name }}`
   * @action  '{{ kebabCase name }}/:{{ kebabCase name }}_id'
   */
  describe('destroy', function() {
    it(`should destroy {{ pascalCase name }}`, function() {
      assert.equal(true, true)
    })
  })

})