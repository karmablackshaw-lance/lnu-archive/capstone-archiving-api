/*
 * TODO - Enter handler description here...
 * Validations and conditions are applied here...
 * @handler  {{ pascalCase name }}
*/

const router = require('koa-router')()

// store
const {{ camelCase name }} = require('@store/{{ kebabCase name }}')

// utilities

// libraries
const Joi = require('joi')
const _get = require('lodash/get')

// middlewares

router
  .prefix('/{{ kebabCase name }}')

  .get('/', async ctx => {
    try {
      const params = {
        ...ctx.getDefaults()
      }

      const count = await {{ camelCase name }}.index({ ...params, isCount: 1 })
      const list = await {{ camelCase name }}.index({ ...params })

      ctx.body = { count: _get(count, 'total', 0), list }
    } catch (error) {
      ctx.throw(error)
    }
  })

  .post('/', async ctx => {
    const schema = Joi.object({
      //
    })

    try {
      const request = await schema.validateAsync(ctx.request.body)

      await {{ camelCase name }}.store({
        //
      })

      ctx.status = 200
    } catch (error) {
      ctx.throw(error)
    }
  })

module.exports = router
