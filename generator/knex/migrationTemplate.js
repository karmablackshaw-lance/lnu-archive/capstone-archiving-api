exports.up = async knex => {
  await knex.schema.dropTableIfExists('{{ snakeCase name }}')
  await knex.schema.createTable('{{ snakeCase name }}', table => {
    table.increments('id').primary()
    table.dateTime('created_at').notNullable().defaultTo(knex.raw('CURRENT_TIMESTAMP'))
    table.dateTime('updated_at').defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))
    table.dateTime('deleted_at').defaultTo(knex.raw('NULL'))
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('{{ snakeCase name }}')
}
