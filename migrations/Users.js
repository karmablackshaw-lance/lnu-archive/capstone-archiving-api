exports.up = async knex => {
  await knex.schema.dropTableIfExists('users')
  await knex.schema.createTable('users', table => {
    table.increments('id')
      .primary()

    table.string('fname')
      .notNullable()

    table.string('mname')
      .notNullable()

    table.string('lname')
      .notNullable()

    table.enum('level', ['admin', 'student'])
      .defaultTo('student')

    table.string('username')
      .unique()
      .notNullable()

    table.string('password')
      .notNullable()

    table.string('temp')
      .notNullable()

    table.string('color')
      .defaultTo('primary')

    table.enum('status', ['pending', 'confirmed', 'rejected'])
      .defaultTo('pending')

    table.dateTime('created_at')
      .notNullable()
      .defaultTo(knex.raw('CURRENT_TIMESTAMP'))

    table.dateTime('updated_at')
      .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))

    table.dateTime('deleted_at')
      .defaultTo(knex.raw('NULL'))
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('users')
}
