exports.up = async knex => {
  await knex.schema.dropTableIfExists('archives')
  await knex.schema.createTable('archives', table => {
    table.increments('id')
      .primary()

    table.string('title')
      .notNullable()

    table.json('authors')

    table.string('adviser')
      .notNullable()

    table.string('path')
      .notNullable()

    table.string('note')
      .notNullable()

    table.json('panelists')
      .notNullable()

    table.json('tags')
      .notNullable()

    table.dateTime('published_date')

    table.dateTime('created_at')
      .notNullable()
      .defaultTo(knex.raw('CURRENT_TIMESTAMP'))

    table.dateTime('updated_at')
      .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))

    table.dateTime('deleted_at')
      .defaultTo(knex.raw('NULL'))
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('archives')
}
