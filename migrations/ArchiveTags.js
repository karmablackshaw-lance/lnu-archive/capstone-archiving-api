exports.up = async knex => {
  await knex.schema.dropTableIfExists('archive_tags')
  await knex.schema.createTable('archive_tags', table => {
    table.increments('id')
      .primary()

    table.integer('archive_id', 10)
      .unsigned()
      .notNullable()

    table.integer('tag_id', 10)
      .unsigned()
      .notNullable()

    table.dateTime('created_at')
      .notNullable()
      .defaultTo(knex.raw('CURRENT_TIMESTAMP'))

    table.dateTime('updated_at')
      .defaultTo(knex.raw('NULL ON UPDATE CURRENT_TIMESTAMP'))

    table.dateTime('deleted_at')
      .defaultTo(knex.raw('NULL'))
  })
}

exports.down = knex => {
  return knex.schema.dropTableIfExists('archive_tags')
}
