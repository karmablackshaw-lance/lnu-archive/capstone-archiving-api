exports.up = async knex => {
  await knex.schema.alterTable('archive_tags', table => {
    table
      .foreign('archive_id')
      .references('archives.id')
      .onDelete('cascade')

    table
      .foreign('tag_id')
      .references('tags.id')
      .onDelete('cascade')
  })
}

exports.down = async knex => {
  await knex.raw('SET FOREIGN_KEY_CHECKS=0')
  await knex.schema.alterTable('archive_tags', table => {
    table.dropForeign('archive_id')
    table.dropForeign('tag_id')
  })
  await knex.raw('SET FOREIGN_KEY_CHECKS=1')
}
