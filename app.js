/**
 * Application starting point
 * Boostrap services here
 */

const cluster = require('cluster')
const cpus = require('os').cpus().length
require('dotenv').config()
require('module-alias/register')

const bootstrap = () => {
  require('@utilities/knex')
  // require('@utilities/redis').start()
}

if (cluster.isMaster) {
  bootstrap()
  // require('./tasks')()

  for (let i = 0; i < cpus; i++) {
    cluster.fork()
  }
} else {
  bootstrap()
  require('./services')()
    .catch(console.error)
}
